---
layout: default
---
## What you will find here ([holoContent][url])


* our [holoIndex](https://share.clickup.com/d/h/2c70v-470/06a69d26e8db6ba)
* a [README](README)

note the full complete repository is encrypt using [cryfs][1] (AES-256-GCM)
with a key from [scrypt][2]
and you would need a [config][4] file that hold the keys.

see also [*](https://www.cryfs.org/comparison/)

[url]: https://holosphere4.gitlab.io/holoMedia

--&nbsp;<br>
(page source: {{page.name}})


[1]: https://www.cryfs.org/howitworks/
[2]: https://en.wikipedia.org/wiki/Scrypt
[3]: https://github.com/cryfs/cryfs
[4]: secure.tgz.bit
